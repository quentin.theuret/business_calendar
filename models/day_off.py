# coding: utf-8
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-TODAY Quentin THEURET (<http://quentin-theuret.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime

from openerp import models, fields, api
from openerp.exceptions import ValidationError
from openerp.tools.translate import _

from openerp.addons.business_calendar.tools.easter import Easter


MONTHS = [
    (1, _(u'January')),
    (2, _(u'February')),
    (3, _(u'March')),
    (4, _(u'April')),
    (5, _(u'May')),
    (6, _(u'June')),
    (7, _(u'July')),
    (8, _(u'August')),
    (9, _(u'September')),
    (10, _(u'October')),
    (11, _(u'November')),
    (12, _(u'December')),
]


def get_max_day(month, year):
    max_day = 31
    if month == 2:
        if not year or ((year % 4 == 0 and year % 100 != 0) or year % 400 == 0):
            max_day = 29
        else:
            max_day = 28
    elif month in (4, 6, 9, 11):
        max_day = 30

    return max_day


class DayOffDef(object):

    def __init__(self, date, name, is_holiday, is_nonworking):
        self.__date = date
        self.__name = name
        self.__is_holiday = is_holiday
        self.__is_nonworking = is_nonworking

    @property
    def date(self):
        return self.__date

    @property
    def name(self):
        return self.__name

    @property
    def is_holiday(self):
        return self.__is_holiday

    @property
    def is_nonworking(self):
        return self.__is_nonworking


class DayOff(models.Model):
    _name = 'resource.day.off'
    _description = u'Days off for the business calendar'
    __days_off_cache = {}

    """
    Methods to compute field values
    """
    def _get_display_name(self):
        """
        The display name of a day off is the concatenation of its name and the real date
        :return: For each business.day.off, apply its display name
        """
        for record in self:
            if record.computation_mode != 'extra':
                record.display_name = u'%s' % (record.name, )
                for_year = self.env.context.get('compute_year', False)
                if for_year:
                    record.display_name += ' (%s)' % (
                        datetime.datetime.strftime(record.__calc_date(for_year), '%d/%m/%Y'),
                    )
            else:
                record.display_name = u'%s (%s)' % (
                    record.name,
                    datetime.datetime.strftime(fields.Date.from_string(record.date), '%d/%m/%Y'),
                )

        return True

    def _get_date(self):
        """
        Returns the date of the day off if it's an Extra Day Off
        :return: For each business.day.off record, the date of the day off or False
        """
        for record in self:
            record.date = datetime.date(year=record.year,
                                        month=record.month,
                                        day=record.day) if record.computation_mode == 'extra' else False

    def _set_date(self):
        """
        Set the date of the day off with the given date.
        If the record is an Extra Day Off, returns an error as this type of days off cannot be modified
        :return: For each business.day.off record, the date of the day off
        """
        for record in self:
            if record.date and record.computation_mode != 'extra':
                raise Exception(
                    _(u'You cannot change the date for an Extra Day Off'),
                )
            if record.date:
                value = fields.Date.from_string(record.date)
                record.write({
                    'day': value.day,
                    'month': value.month,
                    'year': value.year,
                })

    """
    Fields definition
    """
    name = fields.Char(string=u'Day name', required=True)
    active = fields.Boolean(string=u'Active', default=True)
    sequence = fields.Integer(string=u'Sequence', default=100)

    computation_mode = fields.Selection(
        string=u'Computation mode',
        selection=[
            ('fixed', u'Fixed date each year'),
            ('easter', u'Computed according to easter'),
            ('extra', u'Extra day off'),
        ],
        default='extra',
        help=u"""Select the computation mode for the day off :
    * 'Fixed date each year': please fill the day and the month for this day off,
    * 'Computed according to easter': please fill the number of days from/to easter (+/-),
    * 'Extra day off': please fill a date in the DD/MM/YYYY format.
        """
    )
    day = fields.Integer(string=u'Day')
    month = fields.Selection(selection=MONTHS, string=u'Month')
    year = fields.Integer(string=u'Year')

    date = fields.Date(string=u'Date', compute=_get_date, inverse=_set_date)
    display_name = fields.Char(compute=_get_display_name, string=u'Description')

    is_holiday = fields.Boolean(u'Official holiday')
    is_nonworking = fields.Boolean(u'Non-working day', default=True)

    """
    View methods
    """
    @api.constrains('day', 'month', 'year', 'computation_mode')
    def check_settings(self):
        """
        Make checks on given date to avoid bad date
        :return: None
        """
        for record in self:
            if record.computation_mode == 'fixed':
                if record.year:
                    raise ValidationError(
                        _(u'You cannot fill the year in \'Fixed date each year\' mode.'),
                    )
                if record.month < 1 or record.month > 12:
                    raise ValidationError(
                        _(u'Please enter a valid month number (from 1 to 12)'),
                    )

                max_day = get_max_day(record.month, record.year)
                if record.day < 1 or record.day > max_day:
                    raise ValidationError(
                        _(u'Please enter a valid day number (from 1 to %s)') % max_day,
                    )
            elif record.computation_mode == 'easter':
                if record.year or record.month:
                    raise ValidationError(
                        _(u'You cannot enter year or month in the \'Computed according to easter\' mode.'),
                    )

    @api.onchange('computation_mode')
    def _mode_changed(self):
        """
        In case of 'Fixed day each year', if day is not defined, fill it with 1
        :return: None
        """
        if self.computation_mode == 'fixed' and self.day == 0:
            self.day = 1

    """
    Controller methods
    """
    @classmethod
    def reset_days_off_cache(cls, year=None):
        """
        Un-fill cache of days off
        :param year: If defined, only un-fill cache for the given year
        :return: None
        """
        if year is None:
            cls.__days_off_cache = {}
        else:
            cls.__days_off_cache[year] = {}

    @api.model
    def create(self, values):
        """
        Reset cache of days off when a new day off is created
        :param values: Values to put on the new day off
        :return: The ID of the new day off
        """
        self.reset_days_off_cache()
        return super(DayOff, self).create(values)

    @api.multi
    def write(self, values):
        """
        Reset the cache of days off when a new day off is modified
        :param values: Values to update
        :return: True if all is ok
        """
        self.reset_days_off_cache()
        return super(DayOff, self).write(values)

    def __calc_date(self, for_year):
        """
        Compute the date for the current day off according to current year
        :param for_year: Year of computation
        :return:  A datetime.date instance
        """
        if self.computation_mode == 'extra':
            d = datetime.date(year=self.year or 0, month=self.month or 0, day=self.day)
        elif self.computation_mode == 'fixed':
            d = datetime.date(year=for_year, month=self.month or 1, day=self.day or 1)
        elif self.computation_mode == 'easter':
            d = Easter(for_year) + self.day
        return d

    def get_days_off_for_year(self, for_year):
        """
        Compute days off for a specific year and fill the cache for this year
        :param for_year: Specific year on which days off should be computed
        :return: A list of days off for the specific year sorted by date
        """
        days_off = self.__days_off_cache.get(for_year, {})
        if len(days_off) == 0:
            records = self.env[self._name].search(['|', ('year', '=', for_year), ('year', '=', False)])
            for rec in records:
                d = rec.__calc_date(for_year)
                days_off[d] = DayOffDef(d, rec.name, rec.is_holiday, rec.is_nonworking)
            self.__days_off_cache[for_year] = days_off
        return sorted(days_off)

    def get_days_off_iterator(self, from_date=None, to_date=None):
        """
        Return an iterator on days off that are between two dates.
        :param from_date: From date to compute days off
        :param to_date: To date to compute days off
        :return: An iterator on days off
        """
        start = from_date if from_date is not None else datetime.date.today()
        y = start.year
        while (y <= to_date.year or to_date is None) and y > 0:
            days_off = self.get_days_off_for_year(y)
            for d in days_off:
                if to_date is not None and d > to_date:
                    y = -1
                    break
                if d >= start:
                    yield self.get_day_off_desc(d)
            y += 1

    def get_days_off(self, date_from, date_to):
        """
        Return a dictionary with the date of the day off and the name of the day off for days off between two dates
        :param date_from: From date to compute days off
        :param date_to: To date to compute days off
        :return: A dictionary of days off
        """
        return {fields.Date.to_string(item.date): item.name for item in self.get_days_off_iterator(
                    fields.Date.from_string(date_from),
                    fields.Date.from_string(date_to))
                if item.is_nonworking}

    def get_day_off_desc(self, date):
        """
        Get day off from a given date
        :param date: Date of the day off
        :return: A day off
        """
        return self.__days_off_cache[date.year][date]