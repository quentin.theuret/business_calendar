# coding: utf-8
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-TODAY Quentin THEURET (<http://quentin-theuret.net>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program. If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime


class Easter(datetime.date):

    def __new__(cls, year=None):

        if year is None:
            year = datetime.date.today().year

        a = year // 100
        b = year % 100
        c = (3 * (a + 25)) // 4
        d = (3 * (a + 25)) % 4
        e = (8 * (a + 11)) // 25
        f = (5 * a + b) % 19
        g = (19 * f + c - e) % 30
        h = (f + 11 * g) // 319
        j = (60 * (5 - d) + b) // 4
        k = (60 * (5 - d) + b) % 4
        m = (2 * j - k - g + h) % 7
        n = (g - h + m + 114) // 31
        p = (g - h + m + 114) % 31

        return super(Easter, cls).__new__(cls, year, n, p + 1)

    def __add__(self, other):
        if type(other) is int:
            return self + datetime.timedelta(other)
        else:
            return super(Easter, self).__add__(other)

    def __sub__(self, other):
        if type(other) is int:
            return self - datetime.timedelta(other)
        else:
            return super(Easter, self).__sub__(other)
